const fs = require('fs');

function problem2()
{
    // Reading lipsum.txt file and storing content in data
    fs.readFile('lipsum.txt', {encoding: 'utf-8'}, (err, data) => {
        if(err)
        {
            throw(err);
        }
        else
        {
            // Converting contnets of data in uppsercase and writing it to new File.
            fs.writeFile('WrittenInUpperCase.txt', data.toUpperCase(), (err) => {
                if(err)
                {
                    throw(err);
                }
                else
                {
                    // Writingthe name of the created file to filenames.txt
                    fs.writeFile('filenames.txt', 'WrittenInUpperCase.txt', (err) => {
                        if(err)
                        {
                            throw(err);
                        }
                        else
                        {
                            console.log("FileName Written");
                            console.log("\n");
                        }
                    })
                    // Reading contents of WrittenInUpperCase.txt and storing it in lowerCaseData
                    fs.readFile('WrittenInUpperCase.txt', {encoding: 'utf-8'}, (err, lowerCaseData) => {
                        if(err)
                        {
                            throw(err);
                        }
                        else
                        {
                            console.log(lowerCaseData);
                            console.log('\n')
                            lowerCaseData = lowerCaseData.toLowerCase(); // Converting to lowercase
                            let splitsent = lowerCaseData.split("."); // Splitting the string into sentences

                            // Writing array type data into splitSentenses.txt by creating a write stream.
                            let file = fs.createWriteStream('splitSentenses.txt');
                            file.on('error', function(err) { Console.log(err) });
                            splitsent.forEach(value => file.write(`${value}\r\n`));
                            file.end();
                            
                            // Adding new filename to filenames.txt
                            fs.appendFile('filenames.txt', ' splitSentenses.txt', (err) => {
                                if(err)
                                {
                                    throw(err);
                                }
                                else
                                {
                                    console.log("FileName Written");
                                    console.log("\n");
                                }

                                // Reading contents of splitSentenses.txt and storing it in tobeSorted
                                fs.readFile('splitSentenses.txt', {encoding: 'utf-8'}, (err, toBeSorted) => {
                                    if(err)
                                    {
                                        throw(err);
                                    }
                                    else
                                    {
                                        toBeSorted = Array.of(toBeSorted);
                                        let sortedVal = toBeSorted.sort();
                                        sortedVal = sortedVal.toString();
                                        
                                        // Writing contents of sortedVal in new file.
                                        fs.writeFile('sortedSentence.txt', sortedVal, (err) => {
                                            if(err)
                                            {
                                                throw(err);
                                            }
                                            else
                                            {
                                                console.log("Data Written");
                                                console.log("\n");

                                                // Adding new filename to filenames.txt
                                                fs.appendFile('filenames.txt', ' sortedSentence.txt', (err) => {
                                                    if(err)
                                                    {
                                                        throw(err);
                                                    }
                                                    else
                                                    {
                                                        console.log("FileName Written");
                                                    }

                                                    // Reading contents of filenames.txt and storing in NameOfFiles
                                                    fs.readFile('filenames.txt', {encoding: 'utf-8'}, (err, NameOfFiles) => {
                                                        if(err)
                                                        {
                                                            throw(err);
                                                        }
                                                        else
                                                        {

                                                            NameOfFiles =  NameOfFiles.split(" "); //Splitting the content to get array of data.
                                                            console.log(NameOfFiles);
                                                            console.log('\n');

                                                            // Removing the filenames stored in NameOfFiles
                                                            NameOfFiles.map((element) => {
                                                                let filename = element;
                                                                fs.unlink(filename, (err) => {
                                                                if(err)
                                                                {
                                                                    throw (err);
                                                                }
                                                                else
                                                                {
                                                                    console.log("File Deleted " + filename);
                                                                }
                                                                })
                                                                return element;
                                                            })
                                                        }
                                                    })
                                                })
                                            }
                                        })
                                    }
                                })
                            })
                        }
                    })
                }
            })
        }
    })
}


module.exports = problem2;