const fs = require('fs');

// FUnction to create files with deleteFiles as parameter tot it

// function mainfunction(makeDIrectory, createFiles, deletefiles, deleteDirectory)
// {
//     makeDIrectory(createFiles, deletefiles, deleteDirectory);
// }

// FUnction to make a directory
function makeDIrectory(createFiles, delete_Files, deleteDirectory)
{
    fs.mkdir("./New_Directory", function(err) {
        if(err) 
        {
          console.log(err)
        } 
        else 
        {
          console.log("New directory successfully created.")
        }
      })

      createFiles(delete_Files, deleteDirectory);
}


// FUnction to create files.
function createFiles(delete_Files, deleteDirectory)
{
        
    for(let count = 1; count<=3; count++)
    {
        let filename = 'newFile'+count+'.js'
        fs.open(filename, 'w', (err) => {
        if(err)
        {
            throw (err);
        }
        else
        {
            console.log("File Created " + filename)
        }
        })
    }
          
        delete_Files(deleteDirectory);
}

// Function to delete files. 
function delete_file(deleteDirectory)
{
    for(let count = 1; count<=3; count++)
    {
        let filename = 'newFile'+count+'.js'
        fs.unlink(filename, (err) => {
        if(err)
        {
            throw (err);
        }
        else
        {
            console.log("File Deleted " + filename);
        }
        })
    }

    deleteDirectory();
}

// FUnction to Delete a DIrectory. 
function deleteDirectory()
{
    fs.rmdir('./New_Directory', (err) => {
        if(err)
        {
            throw(err);
        }
        else
        {
            console.log("Directory Successfully Deleted!");
        }
    })
}

module.exports = {makeDIrectory, createFiles, delete_file, deleteDirectory};



