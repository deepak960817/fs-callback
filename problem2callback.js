const fs = require('fs');

function problem2(readfile, writefile, writeFilenames)
{
    const output1 = readfile('lipsum.txt');
    
    const upperCaseData = output1.toUpperCase();

    writefile('WrittenInUpperCase.txt', upperCaseData);

    writeFilenames(' WrittenInUpperCase.txt');

    const lowerCaseData = output2.toLowerCase();
    let splitsent = lowerCaseData.split("."); // Splitting the string into sentences

    writeArrayData('splitSentenses.txt',splitsent);

    writeFilenames(' splitSentenses.txt');

    const output2 = readfile('splitSentenses.txt');

    output2 = Array.of(output2);
    let sortedVal = output2.sort();
    sortedVal = sortedVal.toString();

    writefile('sortedSentence.txt', sortedVal);

    writeFilenames(' sortedSentence.txt');

    let filesPresent = readfile(filenames.txt)

    NameOfFiles =  filesPresent.split(" "); //Splitting the content to get array of data.
    console.log(NameOfFiles);
    console.log('\n');

    NameOfFiles.map((element) => {
        let filename = element;
        fs.unlink(filename, (err) => {
        if(err)
        {
            throw (err);
        }
        else
        {
            console.log("File Deleted " + filename);
        }
        })
        return element;
    })
}

function readfile(filename)
{
    //console.log(filename);
    fs.readFile(filename, {encoding: 'utf-8'}, (err, data) => {
        if(err)
        {
            throw(err);
        }
        else
        {
            //console.log(data);
            console.log(filename +" Read");
            return data;
        }
    })
}

function writefile(filename, data)
{
    if(typeof data === 'string')
    {
        fs.writeFile(filename, data, (err) => {
        if(err)
        {
            throw(err);
        }
        else
        {
            console.log("Written in file");
            //return data;
        }
        })
    }
    else if(typeof data === 'object')
    {
        let file = fs.createWriteStream(filename);
        file.on('error', function(err) { console.log(err) });
        data.forEach(value => file.write(`${value}\r\n`));
        file.end();
    }
}

function writeFilenames(filename)
{
    fs.writeFile('filenames.txt', filename, (err) => {
        if(err)
        {
            throw(err);
        }
        else
        {
            console.log("FileName Written");
            console.log("\n");
        }
    })
}


module.exports = {readfile, writefile, writeFilenames, problem2};